package com.huike.aspectj;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.huike.common.annotation.Log;
import com.huike.common.enums.BusinessType;
import com.huike.config.ThreadPoolConfig;
import com.huike.domain.system.SysOperLog;
import com.huike.domain.system.dto.LoginUser;
import com.huike.mapper.SysOperLogMapper;
import com.huike.service.ISysOperLogService;
import com.huike.service.impl.SysOperLogServiceImpl;
import com.huike.utils.ServletUtils;
import com.huike.utils.ip.AddressUtils;
import com.huike.utils.ip.IpUtils;
import com.huike.utils.spring.SpringUtils;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.*;

@Slf4j
@Aspect
@Component
public class OperLogAspect {

    @Autowired
    private ISysOperLogService sysOperLogService;

    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(myLog)")
    public Object OperLog(ProceedingJoinPoint proceedingJoinPoint, Log myLog) throws Throwable {

        Object result = null;
        try {
            result = proceedingJoinPoint.proceed();
            handLog(proceedingJoinPoint,myLog,result,null);
        } catch (Throwable throwable) {
//            throwable.printStackTrace();
            handLog(proceedingJoinPoint,myLog,null,throwable);
            throw throwable; // 继续往上抛
        }
        return result;
    }

    private void handLog(ProceedingJoinPoint proceedingJoinPoint, Log myLog, Object result, Throwable throwable) {
        // title
        String title = myLog.title();
//        log.info("title:{}", title);
        // business_type
        BusinessType businessType = myLog.businessType();
//        log.info("businessType:{}", businessType.ordinal());
        // method 请求方法
        String signature = proceedingJoinPoint.getSignature().getDeclaringTypeName();
        String name = proceedingJoinPoint.getSignature().getName();
        String method = signature + "." + name;
//        log.info("method:{}", method);
        // request_method
        HttpServletRequest request = ServletUtils.getRequest();
        String requestMethod = request.getMethod();
//        log.info("requestMethod---{}", requestMethod);
        // operator_type 操作类别，0其他 1后台用户 2手机用户
        String header = request.getHeader("User-Agent");
        Integer operatorType = 0;
        if (header != null) {
            // 判断是否为手机用户
            if (header.contains("Android") || header.contains("iPhone")) {
                operatorType = 2;
            }
            // 判断是否为 PC 后台用户
            else if (header.contains("Windows NT")) {
                operatorType = 1;
            }
        }
//        log.info("type:{}", operatorType);
        // oper_name
        LoginUser loginUser = SpringUtils.getBean(TokenService.class).getLoginUser(request);
        String operName = loginUser.getUser().getUserName();
//        log.info("operName----{}", operName);
        // dept_name
        String deptName = loginUser.getUser().getDept() != null ? loginUser.getUser().getDept().getDeptName() : null;
//        log.info("depeName---{}", deptName);
        // oper_url
        String operUrl = request.getRequestURI();
//        log.info("operurl:{}", operUrl);
        // oper_ip
//        String localAddr = request.getLocalAddr();
        String ip = IpUtils.getIpAddr(request);
//        log.info("operIp:{}", ip);
        // oper_location
        String address = AddressUtils.getRealAddressByIP(ip);
//        log.info("oper_location:{}", address);
        // oper_param
        Object[] args = proceedingJoinPoint.getArgs();
        String param = Arrays.toString(args);
//        log.info("oper_param:{}", param);

        // status
        Integer status = 0;
        if (throwable != null){
            status = 1;
        }
//        log.info("status:{}", status);

        // json_result
        String jsonString = null;
        if (result != null) {
            jsonString = JSONObject.toJSONString(result);
//            log.info("jsonResult:{}", jsonString);
        }

        // error_msg
        String errorMsg = null;
        if (throwable != null) {
            errorMsg = throwable.getMessage();
//            log.info("errorMsg:{}", errorMsg);
        }
        // oper_time
        Date operTime = new Date();
//        log.info("operTime:{}", operTime);



        SysOperLog sysOperLog = new SysOperLog();
        sysOperLog.setTitle(title);
        sysOperLog.setBusinessType(businessType.ordinal());
        sysOperLog.setMethod(method);
        sysOperLog.setRequestMethod(requestMethod);
        sysOperLog.setOperatorType(operatorType);
        sysOperLog.setOperName(operName);
        sysOperLog.setDeptName(deptName);
        sysOperLog.setOperUrl(operUrl);
        sysOperLog.setOperIp(ip);
        sysOperLog.setOperLocation(address);
        sysOperLog.setOperParam(param);
        sysOperLog.setJsonResult(jsonString);
        sysOperLog.setStatus(status);
        sysOperLog.setErrorMsg(errorMsg);
        sysOperLog.setOperTime(operTime);

        // 开启线程进行日志记录
        sysOperLogService.insertOperlog(sysOperLog);
    }

}
