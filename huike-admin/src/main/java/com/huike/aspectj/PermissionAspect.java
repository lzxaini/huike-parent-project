package com.huike.aspectj;

import com.huike.common.annotation.PreAuthorize;
import com.huike.common.constant.HttpStatus;
import com.huike.common.exception.BaseException;
import com.huike.domain.system.SysUser;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.ServletUtils;
import com.huike.utils.StringUtils;
import com.huike.utils.spring.SpringUtils;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Slf4j
@Aspect
@Component
public class PermissionAspect {


    @Around("@annotation(preAuthorize) && execution(* com.huike.controller.*.*.*(..))")
    public Object permission(ProceedingJoinPoint joinPoint, PreAuthorize preAuthorize) throws Throwable {
        log.info("权限控制----------------------------------------------------");

        // 获得注解的value，权限
        String permission = preAuthorize.value();

        // 获得用户的权限
        LoginUser loginUser = SpringUtils.getBean(TokenService.class).getLoginUser(ServletUtils.getRequest());

        if (loginUser == null || loginUser.getPermissions() == null || loginUser.getUser() == null){
            ServletUtils.getResponse().setStatus(HttpStatus.UNAUTHORIZED);
            return null;
        }

        // 获取用户权限
        Set<String> permissions = loginUser.getPermissions();

        SysUser currentUser = loginUser.getUser();

        if (!currentUser.isAdmin()) {
            // 不是管理员
            if (!permissions.contains(permission)){
                // 没有权限
                ServletUtils.getResponse().setStatus(HttpStatus.UNAUTHORIZED);
                return null;
            }
        }

        return joinPoint.proceed();
    }
}
