package com.huike.interceptor;

import com.huike.common.constant.Constants;
import com.huike.common.constant.HttpStatus;
import com.huike.common.constant.ScheduleConstants;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.StringUtils;
import com.huike.utils.redis.RedisCache;
import com.huike.web.service.TokenService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截
 */
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private RedisCache redisCache;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 拦截非法请求，没有令牌或者令牌有误或过期不能访问内部

        // 获取请求携带的令牌
        String token = tokenService.getToken(request);

        if (StringUtils.isEmpty(token)) {
            // 为空 拦截
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }

        // token不为空
        try {
            Claims claims = tokenService.parseToken(token);
            // 令牌校验无误
            // 获取redis中令牌的key值
            String uuid = (String) claims.get(Constants.LOGIN_USER_KEY);
            String userKey = Constants.LOGIN_TOKEN_KEY + uuid;

            // 求value
            LoginUser user = redisCache.getCacheObject(userKey);
            if (ObjectUtils.isEmpty(user)){
                // 说明令牌过期了
                response.setStatus(HttpStatus.UNAUTHORIZED);
                return false;
            }else {
                //没有过期 令牌续期
                tokenService.refreshAndCacheToken(user);
                return true;
            }
        } catch (Exception e) {
            // 令牌校验有误
            e.printStackTrace();
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }






    }
}
