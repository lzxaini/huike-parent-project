package com.huike.task;

import com.huike.domain.business.TbBusiness;
import com.huike.domain.system.SysUser;
import com.huike.mapper.SysDictDataMapper;
import com.huike.mapper.TbAssignRecordMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.utils.EmailUtils;
import com.huike.utils.file.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class BusinessTrackNotifyTask {

    @Autowired
    private TbBusinessMapper tbBusinessMapper; //线索
    @Autowired
    private TbAssignRecordMapper tbAssignRecordMapper; //线索分配人
    @Autowired
    private EmailUtils emailUtils; //发送邮件的工具类
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    @Value("${spring.mail.username}")
    private String from; //发件人

    private static final String BUSINESS_NOTIFY_PREFIX = "business:notify:";

    @Scheduled(cron = "0 0/5 * * * ?")
    public void sendNotifyEmail() throws Exception {
        //1. 查询本次需要跟进的商机
     List<TbBusiness> clueList = tbBusinessMapper.selectByNextTime(LocalDateTime.now(), LocalDateTime.now().plusHours(2), "2");

        if(!CollectionUtils.isEmpty(clueList)){
            for (TbBusiness tbBusiness : clueList) {
                //查询是否已发送
                Object flag = redisTemplate.opsForValue().get(BUSINESS_NOTIFY_PREFIX + tbBusiness.getId());
                if(ObjectUtils.isEmpty(flag)){
                    //2. 查询商机跟进人的信息
                    SysUser sysUser = tbAssignRecordMapper.selectByAssignIdAndType(tbBusiness.getId(), "1");
                    //3. 发送提醒邮件
                    sendMail(tbBusiness, sysUser);
                }
            }
        }
    }

    private void sendMail(TbBusiness tbBusiness, SysUser sysUser) throws Exception {
        //发送邮件
        String text = handleMailText(tbBusiness, sysUser);
        emailUtils.sendMailWithoutAttachment(from, sysUser.getEmail(), "商机跟进提醒", text);

        log.info("发送邮件, 给指定员工: {} , 邮件正文: {}", sysUser.getEmail(), text);

        //存储已发送标识
        redisTemplate.opsForValue().set(BUSINESS_NOTIFY_PREFIX + tbBusiness.getId(), "1", 150, TimeUnit.MINUTES);
    }


    private String handleMailText(TbBusiness tbBusiness, SysUser sysUser) throws IOException {
        //1. 加载邮件的正文模板
        String file = this.getClass().getClassLoader().getResource("template/BusinessNotifyTemplate.html").getFile();
        String text = FileUtils.readFileToString(new File(file), "UTF-8");
        String tbBusinessSubject = tbBusiness.getSubject();
        String subject = sysDictDataMapper.selectDictLabel("course_subject",tbBusinessSubject);
//        if (tbBusinessSubject.equals("0")){
//            subject = "Java";
//        }
//        if (tbBusinessSubject.equals("1")){
//            subject = "前端";
//        }
//        if (tbBusinessSubject.equals("2")){
//            subject = "人工智能";
//        }
//        if (tbBusinessSubject.equals("3")){
//            subject = "大数据";
//        }
//        if (tbBusinessSubject.equals("4")){
//            subject = "Python";
//        }
//        if (tbBusinessSubject.equals("5")){
//            subject = "测试";
//        }
//        if (tbBusinessSubject.equals("6")){
//            subject = "新媒体";
//        }
//        if (tbBusinessSubject.equals("7")){
//            subject = "产品经理";
//        }
//        if (tbBusinessSubject.equals("8")){
//            subject = "UI设计";
//        }


        //2. 填充数据
        return String.format(text, sysUser.getRealName(), tbBusiness.getId(), tbBusiness.getName(), tbBusiness.getPhone(), tbBusiness.getSex().equals("0") ? "男" : "女",subject);
    }

}
