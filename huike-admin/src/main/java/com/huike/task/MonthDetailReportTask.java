package com.huike.task;

import com.huike.domain.business.TbBusiness;
import com.huike.domain.business.dto.BusinessCountDTO;
import com.huike.domain.clues.dto.ClueCountDTO;
import com.huike.domain.contract.dto.ContractCountDTO;
import com.huike.domain.contract.dto.ContractMoneyDTO;
import com.huike.domain.report.vo.DetailReportVo;
import com.huike.domain.system.SysUser;
import com.huike.mapper.*;
import com.huike.utils.EmailUtils;
import com.huike.utils.file.FileUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MonthDetailReportTask {

    @Autowired
    private TbBusinessMapper businessMapper; //商机

    @Autowired
    private TbClueMapper clueMapper;

    @Autowired
    private TbContractMapper contractMapper;

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private EmailUtils emailUtils; //发送邮件的工具类

    @Autowired
    private Configuration configuration;


    @Value("${spring.mail.username}")
    private String from; //发件人


        @Scheduled(cron = "0 0 23 L * ?")
//    @Scheduled(cron = "0/30 * * * * ?")
    public void sendMonthDetailReportEmail() throws Exception {
        // 统计本月新增线索数、新增商机数、新增合同数、销售金额(合同金额)
//        LocalDate beginDate = LocalDate.of(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), 1);
//        LocalDate endDate = YearMonth.now().atEndOfMonth();
        LocalDate beginDate = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate endDate = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());

        List<String> dateList = beginDate.datesUntil(endDate.plusDays(1)).map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());


        LocalDateTime begin = LocalDateTime.of(beginDate, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endDate, LocalTime.MAX);

        // 获取每天线索
        List<ClueCountDTO> clueCountDTOList = clueMapper.countByEveryDay(begin, end);
        Map<String, Integer> clueDataMap = clueCountDTOList.stream().collect(Collectors.toMap(ClueCountDTO::getClueDate, ClueCountDTO::getClueCount));

        //2.2 获取每一天的商机数量
        List<BusinessCountDTO> businessCountDTOList = businessMapper.countByEveryDay(begin, end);
        Map<String, Integer> businessDataMap = businessCountDTOList.stream().collect(Collectors.toMap(BusinessCountDTO::getBusinessDate, BusinessCountDTO::getBusinessCount));

        //2.3 获取每一天的合同数量
        List<ContractCountDTO> contractCountDTOList = contractMapper.countByEveryDay(begin, end);
        Map<String, Integer> contractDataMap = contractCountDTOList.stream().collect(Collectors.toMap(ContractCountDTO::getContractDate, ContractCountDTO::getContractCount));

        //2.4 获取每一天的销售额
        List<ContractMoneyDTO> contractMoneyDTOList = contractMapper.sumMoneyByEveryDay(begin, end);
        Map<String, Double> contractMoneyMap = contractMoneyDTOList.stream().collect(Collectors.toMap(ContractMoneyDTO::getContractDate, ContractMoneyDTO::getContractMoney));


        //1. 准备数据模型
        Map<String, Object> dataModel = new HashMap<>();
        List<DetailReportVo> detailReportVoList = dateList.stream().map(date -> {
            return DetailReportVo.builder()
                    .date(date)
                    .newClueCount(clueDataMap.get(date) == null ? 0 : clueDataMap.get(date))
                    .newBusinessCount(businessDataMap.get(date) == null ? 0 : businessDataMap.get(date))
                    .newContractCount(contractDataMap.get(date) == null ? 0 : contractDataMap.get(date))
                    .saleMoney(contractMoneyMap.get(date) == null ? 0 : contractMoneyMap.get(date))
                    .build();
        }).collect(Collectors.toList());

        dataModel.put("dataList", detailReportVoList);


        //2. 获取模板
        Template template = configuration.getTemplate("MonthAllReport.ftl");

        //3. 生成文件
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, dataModel);
//        System.out.println(result);

        //2. 获取管理员信息
        SysUser sysUser = sysUserMapper.selectUserById(1L);

        //3. 发送提醒邮件
//        String file = this.getClass().getClassLoader().getResource("template/MonthReport.html").getFile();
//        String template = FileUtils.readFileToString(new File(file), "UTF-8");
//
//        String text = String.format(template,
//                clueCount, diffClue * 100/_allClues,
//                businessCount, diffBusiness * 100/_allBusiness,
//                contractCount, diffContracts * 100/_allContracts,
//                Math.round(totalSales), Math.round(diffSales * 100/_totalSales));

        emailUtils.sendMailWithoutAttachment(from, sysUser.getEmail(), "本月公司运营数据-详细信息", text);

        log.info("发送邮件, 给指定员工: {} ", sysUser.getEmail());
    }

}
