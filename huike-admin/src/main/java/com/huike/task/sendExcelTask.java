package com.huike.task;

import com.huike.domain.business.dto.BusinessCountDTO;
import com.huike.domain.clues.dto.ClueCountDTO;
import com.huike.domain.contract.dto.ContractCountDTO;
import com.huike.domain.contract.dto.ContractMoneyDTO;
import com.huike.domain.report.vo.DetailReportVo;
import com.huike.domain.system.SysUser;
import com.huike.mapper.SysUserMapper;
import com.huike.mapper.TbBusinessMapper;
import com.huike.mapper.TbClueMapper;
import com.huike.mapper.TbContractMapper;
import com.huike.utils.EmailUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class sendExcelTask {


    @Autowired
    private TbClueMapper clueMapper;

    @Autowired
    private TbBusinessMapper businessMapper;

    @Autowired
    private TbContractMapper contractMapper;

    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String from; //发件人

    @Autowired
    private SysUserMapper sysUserMapper;

    private JavaMailSender javaMailSender;

    @Scheduled(cron = "0 0 23 L * ?")
//    @Scheduled(cron = "0/30 * * * * ?")
    public void sendMonthExcel() throws Exception {
        //1. 获取本月数据
        //1.1 获取当月的开始时间 与 结束时间
        LocalDate beginDate = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate endDate = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());

        String begin = beginDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String end = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));


//        //1.2 获取本月的线索总数
//        int allClues = clueMapper.countAllClues(begin, end);
//
//        //1.3 获取本月的商机总数
//        int allBusiness = businessMapper.countAllBusiness(begin, end);
//
//        //1.4 获取本月的合同总数
//        int allContracts = contractMapper.countAllContracts(begin, end);
//
//        //1.5 获取本月的销售额
//        Double totalSales = contractMapper.sumAllSalesStatistics(begin, end);
//        if (totalSales == null){
//            totalSales = 0.0;
//        }

        // 获取每天线索
        List<ClueCountDTO> clueCountDTOList = clueMapper.countByEveryDay(LocalDateTime.of(beginDate, LocalTime.MIN), LocalDateTime.of(endDate, LocalTime.MAX));
        Map<String, Integer> clueDataMap = clueCountDTOList.stream().collect(Collectors.toMap(ClueCountDTO::getClueDate, ClueCountDTO::getClueCount));

        //2.2 获取每一天的商机数量
        List<BusinessCountDTO> businessCountDTOList = businessMapper.countByEveryDay(LocalDateTime.of(beginDate, LocalTime.MIN), LocalDateTime.of(endDate, LocalTime.MAX));
        Map<String, Integer> businessDataMap = businessCountDTOList.stream().collect(Collectors.toMap(BusinessCountDTO::getBusinessDate, BusinessCountDTO::getBusinessCount));

        //2.3 获取每一天的合同数量
        List<ContractCountDTO> contractCountDTOList = contractMapper.countByEveryDay(LocalDateTime.of(beginDate, LocalTime.MIN), LocalDateTime.of(endDate, LocalTime.MAX));
        Map<String, Integer> contractDataMap = contractCountDTOList.stream().collect(Collectors.toMap(ContractCountDTO::getContractDate, ContractCountDTO::getContractCount));

        //2.4 获取每一天的销售额
        List<ContractMoneyDTO> contractMoneyDTOList = contractMapper.sumMoneyByEveryDay(LocalDateTime.of(beginDate, LocalTime.MIN), LocalDateTime.of(endDate, LocalTime.MAX));
        Map<String, Double> contractMoneyMap = contractMoneyDTOList.stream().collect(Collectors.toMap(ContractMoneyDTO::getContractDate, ContractMoneyDTO::getContractMoney));

        List<String> dateList = beginDate.datesUntil(endDate.plusDays(1)).map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());

        List<DetailReportVo> detailReportVoList = dateList.stream().map(date -> {
            return DetailReportVo.builder()
                    .date(date)
                    .newClueCount(clueDataMap.get(date) == null ? 0 : clueDataMap.get(date))
                    .newBusinessCount(businessDataMap.get(date) == null ? 0 : businessDataMap.get(date))
                    .newContractCount(contractDataMap.get(date) == null ? 0 : contractDataMap.get(date))
                    .saleMoney(contractMoneyMap.get(date) == null ? 0 : contractMoneyMap.get(date))
                    .build();
        }).collect(Collectors.toList());

        String dateRange = beginDate.toString()+"到"+endDate.toString();// 时间


        Integer allClues = clueCountDTOList.stream().map(ClueCountDTO::getClueCount).reduce(0, Integer::sum);
        Integer allBusiness = businessCountDTOList.stream().map(BusinessCountDTO::getBusinessCount).reduce(0, Integer::sum);
        Integer allContracts = contractCountDTOList.stream().map(ContractCountDTO::getContractCount).reduce(0, Integer::sum);
        Double totalSales = contractMoneyDTOList.stream().map(ContractMoneyDTO::getContractMoney).reduce(0.0, Double::sum);


        // 加载excel文件进行填充
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream("templates/客达天下-月度运营统计.xlsx");
        Workbook workbook = new XSSFWorkbook(resource);

        Sheet sheet = workbook.getSheetAt(0);

        sheet.getRow(1).getCell(1).setCellValue(dateRange);

        Row row = sheet.getRow(3);
        row.getCell(1).setCellValue(allClues);
        row.getCell(2).setCellValue(allBusiness);
        row.getCell(3).setCellValue(allContracts);
        row.getCell(4).setCellValue(totalSales);

        for (int i = 0; i < detailReportVoList.size(); i++) {
            Row sheetRow = sheet.getRow(5 + i);
            sheetRow.getCell(0).setCellValue(i+1);
            sheetRow.getCell(1).setCellValue(detailReportVoList.get(i).getDate());
            sheetRow.getCell(2).setCellValue(detailReportVoList.get(i).getNewClueCount());
            sheetRow.getCell(3).setCellValue(detailReportVoList.get(i).getNewBusinessCount());
            sheetRow.getCell(4).setCellValue(detailReportVoList.get(i).getNewContractCount());
            sheetRow.getCell(5).setCellValue(detailReportVoList.get(i).getSaleMoney());
        }

//        workbook.write(new FileOutputStream(new File("E:/客达天下-月度运营统计.xlsx")));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);

        SysUser sysUser = sysUserMapper.selectUserById(1L);

        JavaMailSender javaMailSender = emailUtils.getJavaMailSender();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
        messageHelper.setFrom(from);//发件人
        messageHelper.setTo(sysUser.getEmail());//收件人
        messageHelper.setSubject("月度运营统计");//主题
        messageHelper.setText("附件是本月运营数据报表，请查收，数据统计方面有任何问题请联系技术人员确认，谢谢");//内容
//        messageHelper.addAttachment("月度运营统计报表-明细.xlsx",new File("E:/客达天下-月度运营统计.xlsx"));// 附件

        messageHelper.addAttachment("月度运营统计报表-明细.xlsx",new ByteArrayResource(outputStream.toByteArray()));

        javaMailSender.send(mimeMessage);

        workbook.close();

    }
}
