package com.huike.test;

import org.junit.jupiter.api.Test;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;

public class MinIOTest {

    @Test
    public void testUpload(){
        try {
            // Create a minioClient with the MinIO server playground, its access key and secret key.
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://127.0.0.1:9000")
                            .credentials("SPbZKSMksaHb2SsO8AXn", "4axDIZQg0l1ivMQAaGWfns1Lb0jwexvvJD0OBmxT")
                            .build();

            // Make 'asiatrip' bucket if not exist.
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("crm").build());
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("crm").build());
            }

            // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
            // 'asiatrip'.
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket("crm")
                            .object("1.jpg")
                            .filename("D:\\Desktop\\黑马\\JavaReceiptDocument\\1.java黄埔班\\3.javaweb\\day01-HTML-CSS\\资料\\3. 图片、音频、视频\\img\\1.jpg")
                            .build());
        } catch (Exception e) {
            System.out.println("Error occurred: " + e);
        }
    }

}